<?php
// membuat random text dari 0 - 9999
$code=rand(0,9999);

// menyimpan random text pada session
session_start();
 $_SESSION["captcha"]=$code;

// mengubah text menjadi gambar
$im = imagecreate(150, 40);

$bg = imagecolorallocate($im, 0, 0, 0);
$fg = imagecolorallocate($im, 0, 255, 0);
$font = 5;

imagestring($im, $font, 50, 10, $code, $fg);

header('Content-type: image/png');
imagepng($im);

imagedestroy($im);
?>
