-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 16. Agustus 2015 jam 18:19
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sino`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `belajar`
--

CREATE TABLE IF NOT EXISTS `belajar` (
  `id_belajar` int(5) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(5) DEFAULT NULL,
  `id_mapel` varchar(10) DEFAULT NULL,
  `aktif` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id_belajar`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `belajar`
--

INSERT INTO `belajar` (`id_belajar`, `id_siswa`, `id_mapel`, `aktif`) VALUES
(9, 5, 'PRODESK-XI', 'yes'),
(13, 5, 'WEB-XII', 'yes'),
(12, 6, 'PRODESK-XI', 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita_sistem`
--

CREATE TABLE IF NOT EXISTS `berita_sistem` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `isi` text,
  `gambar` tinytext,
  `tanggal` date DEFAULT '0000-00-00',
  `aktif` enum('0','1') DEFAULT NULL COMMENT '0=tidak aktif, 1=aktif',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `berita_sistem`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `id_mapel` varchar(10) NOT NULL,
  `nama_mapel` varchar(50) DEFAULT NULL,
  `id_guru` int(5) DEFAULT NULL,
  `aktif` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id_mapel`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `nama_mapel`, `id_guru`, `aktif`) VALUES
('PRODESK-XI', 'Pemrograman Desktop XI', 2, 'yes'),
('WEB-XII', 'Web Desain XII', 2, 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `materi`
--

CREATE TABLE IF NOT EXISTS `materi` (
  `id_file` int(5) NOT NULL AUTO_INCREMENT,
  `id_mapel` varchar(10) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `file` varchar(100) DEFAULT NULL,
  `tgl_post` date DEFAULT NULL,
  `hit` int(3) DEFAULT NULL,
  `aktif` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data untuk tabel `materi`
--

INSERT INTO `materi` (`id_file`, `id_mapel`, `judul`, `keterangan`, `file`, `tgl_post`, `hit`, `aktif`) VALUES
(3, 'PRODESK-XI', 'test lagi bro ya', 'test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'Desert.jpg', '2015-01-01', 0, 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_quiz`
--

CREATE TABLE IF NOT EXISTS `nilai_quiz` (
  `id_nilai` int(4) NOT NULL AUTO_INCREMENT,
  `id_user` int(5) NOT NULL,
  `id_quiz` int(5) NOT NULL,
  `benar` int(4) NOT NULL,
  `salah` int(4) NOT NULL,
  `kosong` int(4) NOT NULL,
  `score` float NOT NULL,
  `tanggal` datetime NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `nilai_quiz`
--

INSERT INTO `nilai_quiz` (`id_nilai`, `id_user`, `id_quiz`, `benar`, `salah`, `kosong`, `score`, `tanggal`) VALUES
(6, 6, 1, 1, 1, 0, 50, '2015-02-28 16:30:51'),
(2, 5, 1, 1, 1, 0, 50, '2015-02-20 15:13:00'),
(5, 6, 1, 2, 0, 0, 100, '2015-02-24 21:26:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_tugas`
--

CREATE TABLE IF NOT EXISTS `nilai_tugas` (
  `id_nilai` int(5) NOT NULL AUTO_INCREMENT,
  `id_tugas` int(5) DEFAULT NULL,
  `Id_siswa` int(5) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `nilai` float DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `publish` enum('yes','no') DEFAULT NULL,
  `status` enum('s','b') DEFAULT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `nilai_tugas`
--

INSERT INTO `nilai_tugas` (`id_nilai`, `id_tugas`, `Id_siswa`, `file`, `nilai`, `tanggal`, `publish`, `status`) VALUES
(8, 5, 5, 'favicon.gif', 80, '2015-02-20 15:31:42', 'no', 'b'),
(9, 5, 6, 'windows 7 32 bit device manager.jpg', 75, '2015-02-20 15:40:47', 'no', 'b'),
(10, 6, 6, 'Daftar Peserta Didik SMAS AR RISALAH 2014-12-18 10-26-34.xls', 0, '2015-02-28 16:30:24', 'no', 'b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesan`
--

CREATE TABLE IF NOT EXISTS `pesan` (
  `id_pesan` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(20) DEFAULT NULL,
  `penerima` varchar(20) DEFAULT NULL,
  `judul` tinytext,
  `pesan` text,
  `tanggal` date DEFAULT NULL,
  `status` char(1) NOT NULL COMMENT '0=belum dibaca, 1=sudah dibaca',
  PRIMARY KEY (`id_pesan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `id_user`, `penerima`, `judul`, `pesan`, `tanggal`, `status`) VALUES
(3, '2', '6', 'Re: test terus dan terus mas brai', 'balas kepada guru cipto', '2015-03-09', '1'),
(7, '2', '1', 'test', 'piye rud??', '2015-03-09', '1'),
(4, '6', '2', 'Re: Re: test terus dan terus mas brai', 'tak balas pak...\r\npiye pak ??', '2015-03-09', '1'),
(5, '2', '6', 'Re: Re: Re: test terus dan terus mas brai', 'Lha karep mu piye ?', '2015-03-09', '1'),
(6, '6', '2', 'Re: Re: Re: Re: test terus dan terus mas brai', 'Lha piye to pak ?', '2015-03-09', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `quiz`
--

CREATE TABLE IF NOT EXISTS `quiz` (
  `id_quiz` int(5) NOT NULL AUTO_INCREMENT,
  `id_mapel` varchar(10) DEFAULT NULL,
  `judul` text,
  `keterangan` text,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `aktif` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id_quiz`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `quiz`
--

INSERT INTO `quiz` (`id_quiz`, `id_mapel`, `judul`, `keterangan`, `tgl_awal`, `tgl_akhir`, `aktif`) VALUES
(1, 'PRODESK-XI', 'coba quiz', 'coba coba edit<br>', '2015-02-20', '2015-03-07', 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `id_soal` int(10) NOT NULL AUTO_INCREMENT,
  `id_quiz` int(5) DEFAULT NULL,
  `pertanyaan` text,
  `pil_a` text,
  `pil_b` text,
  `pil_c` text,
  `pil_d` text,
  `kunci` varchar(1) DEFAULT NULL,
  `tgl_buat` date DEFAULT NULL,
  `publish` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `soal`
--

INSERT INTO `soal` (`id_soal`, `id_quiz`, `pertanyaan`, `pil_a`, `pil_b`, `pil_c`, `pil_d`, `kunci`, `tgl_buat`, `publish`) VALUES
(1, 1, 'test pertanyaan<br>', 'A', 'B', 'C', 'D', 'A', '2015-01-16', 'yes'),
(2, 1, 'pertanyaan 2<br>', 'A', 'B', 'C', 'D', 'A', '2015-01-16', 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `id_tugas` int(5) NOT NULL AUTO_INCREMENT,
  `id_mapel` varchar(10) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `aktif` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id_tugas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `id_mapel`, `judul`, `deskripsi`, `tgl_awal`, `tgl_akhir`, `aktif`) VALUES
(5, 'PRODESK-XI', 'coba tugas untuk siswa', 'coba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswacoba tugas untuk siswa', '2015-02-20', '2015-03-28', 'yes'),
(6, 'PRODESK-XI', 'test lagi bro', 'test terus brooo<br>', '2015-02-20', '2015-03-28', 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `kode` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nama` varchar(60) DEFAULT NULL,
  `tempat_lhr` varchar(30) DEFAULT NULL,
  `tgl_lhr` date DEFAULT NULL,
  `gender` enum('L','P') DEFAULT NULL,
  `alamat` tinytext,
  `kota_kab` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `foto` tinytext,
  `hak_akses` enum('1','2','3') NOT NULL COMMENT '1 untuk admin, 2 untuk guru, 3 untuk siswa',
  `aktif` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`kode`),
  KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`kode`, `username`, `password`, `nama`, `tempat_lhr`, `tgl_lhr`, `gender`, `alamat`, `kota_kab`, `email`, `hp`, `foto`, `hak_akses`, `aktif`) VALUES
(1, 'rudyekoprasetya@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Rudy Eko Prasetya', 'Tulungagung', '2014-12-02', 'L', 'Kediri', 'Kediri', 'rudyekoprasetya@yahoo.co.id', '085235830024', 'Penguins.jpg', '1', 'yes'),
(2, 'ciptoblues@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Sucipto', 'Kediri', '2014-12-24', 'L', 'Kediri', 'Kediri', 'ciptoblues@gmail.com', '08564567231', 'Yellowflower.jpg', '2', 'yes'),
(4, 'guru@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'Guru 1', '', '2014-12-10', 'P', '', '', '', '', '', '2', 'yes'),
(5, 'siswa@yahoo.co.id', 'e10adc3949ba59abbe56e057f20f883e', 'siswa1', 'kediri', '2014-12-24', 'L', 'kediri', 'kediri', 'siswa@yahoo.co.id', '098767543', 'Koala.jpg', '3', 'yes'),
(6, 'siswa@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'siswa2', 'Kediri', '2015-01-20', 'L', 'kediri', 'kediri', 'siswa@yahoo.com', '', '', '3', 'yes');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
